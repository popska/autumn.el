;;; autumn.el --- Watch leaves fall in Emacs!         -*- lexical-binding: t; -*-

;; Copyright (C) 2018-2020  Adam Porter

;; Author: popska <popska@disroot.org>
;; URL: https://gitlab.com/popska/autumn.el
;; Version: 0.1
;; Package-Requires: ((emacs "26.3"))
;; Keywords: games

;;;; License:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Notes:

;;; Commentary:

;; Watch leaves fall in Emacs! Command `autumn' displays a buffer in
;; which leaves fall from a tree. The wind varies in intensity, a
;; gentle breeze blows at times, and leaves accumulates on the terrain
;; in the scene.

;;; Code:

;;;; Requirements

(require 'cl-lib)
(require 'color)
(require 'subr-x)

;;;; Variables

(defvar autumn-leaves nil)

(defvar autumn-rate 0.09)
(defvar autumn-timer nil)
(defvar autumn-window-width nil)
(defvar autumn-window-height nil)

(defvar autumn-storm-frames nil)
(defvar autumn-storm-reset-frame nil)
(defvar autumn-storm-factor 1)
(defvar autumn-storm-wind 0)

(defvar autumn-leaf-positions nil)

(cl-defstruct autumn-leaf
  x y mass string overlay)

;;;; Customization

(defgroup autumn nil
  "Enjoy the season!"
  :group 'games
  :link '(url-link "https://gitlab.com/popska/autumn.el"))

(defcustom autumn-debug nil
  "Show debug info in mode line."
  :type 'boolean)

(defcustom autumn-pile-factor 5
  "Leaves are reduced in mass by this factor when it hits the ground.
The lower the number, the faster leaves will accumulate."
  :type 'number)

(defcustom autumn-show-background t
  "Show the `autumn-backgrounds' scene."
  :type 'boolean)


(defcustom autumn-leaf-disappear-factor 10
  "How quickly leaves will disappear from the tree. Higher is less
frequent."
  :type 'number)


;; some of the ascii art is from https://asciiart.website/index.php?art=holiday/halloween
;; and https://ascii.co.uk/art/tree
(defcustom autumn-backgrounds
  (list
   (cons 2
	 #("                                                    *         .    
                       *            *              .         ;     
                      .            * .              ;%  *  ;; *    
                     *  ,*          , *            * :;%  %;     * 
                 .      *:  *      ;          *      * :;%;'  *  .,
                ,.        %;    *%; *          ;  *   * %;'  * ,;  
                * ;*      ;%;  %%;      * ,    *%; *  ;%;    ,%' * 
                   %;  *    %;%;  *   ,  ;       %;  ;%; * ,%;'*   
                  * ;%;    * %; &*     ;%;      * % ;%;  ,%;'&     
                     `%;.*    ;%;   * %;'&     *  `;%%;.%;'        
                    * &:;%.*   ;%%. %@; *      %; ;@%;%'  *        
                      *   :%;.* :;bd%;*       * %;@%;'*            
                           `@%:.  :;%.         ;@@%;'              
                          &  `@%.* `;@%.&   * ;@@%; *              
                              *`@%%. `@%% *  ;@@%; *               
                                *;@%. :@%%  %@@%;                  
                                   %@bd%%%bd%%:;                   
                                     #@%%%%%:;;                    
                                     %@@%%%::;                     
                                     %@@@%(o);  . '                
                                     %@@@o%;:(.,'                  
                                 `.. %@@@o%::;                     
       //            _              `)@@@o%::;                     
 .--'``^``'--.       )\\              %@@(o)::;                     
/    #   #    \\   .'`--`'.          .%@@@@%::;                     
|      ^      |  /  ^  ^  \\         ;%@@@@%::;.                    
\\   \\/VvV\\/   /  \\ ^VvvV^ /        ;%@@@@%%:;;;.                   
 `'--,...,--'`    `------`     ...;%@@@@@%%:;;;;,..                
" 0 1902 (face (:foreground "saddle brown")) 52 53 (face (:foreground "dark orange")) 91 92 (face (:foreground "tomato")) 104 105 (face (:foreground "tomato")) 171 172 (face (:foreground "dark orange")) 192 193 (face (:foreground "tomato")) 198 199 (face (:foreground "dark orange")) 225 226 (face (:foreground "dark orange")) 229 230 (face (:foreground "tomato")) 242 243 (face (:foreground "tomato")) 255 256 (face (:foreground "tomato")) 269 270 (face (:foreground "tomato")) 296 297 (face (:foreground "dark orange")) 300 301 (face (:foreground "tomato")) 318 319 (face (:foreground "dark orange")) 325 326 (face (:foreground "dark orange")) 334 335 (face (:foreground "tomato")) 372 373 (face (:foreground "tomato")) 376 377 (face (:foreground "tomato")) 390 391 (face (:foreground "dark orange")) 394 395 (face (:foreground "tomato")) 401 402 (face (:foreground "dark orange")) 424 425 (face (:foreground "tomato")) 427 428 (face (:foreground "tomato")) 448 449 (face (:foreground "tomato")) 455 456 (face (:foreground "dark orange")) 459 460 (face (:foreground "tomato")) 473 474 (face (:foreground "dark orange")) 499 500 (face (:foreground "dark orange")) 510 511 (face (:foreground "tomato")) 533 534 (face (:foreground "tomato")) 539 540 (face (:foreground "tomato")) 562 563 (face (:foreground "dark orange")) 571 572 (face (:foreground "tomato")) 577 578 (face (:foreground "dark orange")) 592 593 (face (:foreground "tomato")) 637 638 (face (:foreground "dark orange")) 648 649 (face (:foreground "dark orange")) 659 660 (face (:foreground "tomato")) 700 701 (face (:foreground "tomato")) 707 708 (face (:foreground "dark orange")) 720 721 (face (:foreground "tomato")) 738 739 (face (:foreground "dark orange")) 770 771 (face (:foreground "dark orange")) 778 779 (face (:foreground "tomato")) 786 787 (face (:foreground "tomato")) 794 795 (face (:foreground "dark orange")) 802 803 (face (:foreground "tomato")) 917 918 (face (:foreground "dark orange")) 928 929 (face (:foreground "tomato")) 936 937 (face (:foreground "dark orange")) 982 983 (face (:foreground "tomato")) 994 995 (face (:foreground "tomato")) 1003 1004 (face (:foreground "dark orange")) 1052 1053 (face (:foreground "dark orange")) 576 577 (face (:foreground "orange")) 605 606 (face (:foreground "orange")) 653 654 (face (:foreground "orange")) 702 703 (face (:foreground "orange")) 910 911 (face (:foreground "orange")) 924 925 (face (:foreground "orange")) 1334 1337 (face (:foreground "#322")) 1604 1607 (face (:foreground "#322")) 1503 1505 (face (:foreground "saddle brown")) 1517 1518 (face (:foreground "saddle brown")) 1565 1578 (face (:foreground "dark orange")) 1585 1587 (face (:foreground "saddle brown")) 1632 1633 (face (:foreground "dark orange")) 1637 1638 (face (:foreground "gold")) 1641 1642 (face (:foreground "gold")) 1646 1647 (face (:foreground "dark orange")) 1650 1658 (face (:foreground "dark orange")) 1700 1701 (face (:foreground "dark orange")) 1707 1708 (face (:foreground "gold")) 1714 1715 (face (:foreground "dark orange")) 1717 1718 (face (:foreground "dark orange")) 1720 1721 (face (:foreground "gold")) 1723 1724 (face (:foreground "gold")) 1726 1727 (face (:foreground "dark orange")) 1768 1769 (face (:foreground "dark orange")) 1772 1779 (face (:foreground "gold")) 1782 1783 (face (:foreground "dark orange")) 1785 1786 (face (:foreground "dark orange")) 1787 1793 (face (:foreground "gold")) 1794 1795 (face (:foreground "dark orange")) 1837 1850 (face (:foreground "dark orange")) 1854 1862 (face (:foreground "dark orange"))
           )) ;; id: 341
;;    (cons 96
;;          #("                                    _____
;;                                _.-\"\"     \"\"-._
;;                              .'               '.
;;                 . . . . .  .'                   '.
;;                 !-!-!-!-! /   .-..                \\
;;                 !_!,!_!_!/    |__H _    .-\\_)`-.   \\
;;               ,/`,/'_`\\,`\\,  [____|_]  /.-. .-,_\\   ;
;;             ,/',/'/_|_\\`\\,`\\,|=   |=|      \\(       ;
;;           ,/' |/ ||\"\"\"|| \\| `\\, = | |       `       |
;;           |   #| ||___|| | #  |=  | |               |
;;         ,/' #  | [_____] |   #`\\, |=|               ;
;;       ,/',-----'      =  '-----,`\\--'---,/\\,--,    /
;;      `\"\"|   .;;;,=      ,;;;,   |#  # ,//  \\\\,'\\, /
;;         | =//___\\\\ =   //___\\\\ =| # ,//',;;,'\\\\,#\\,
;;         |  ||   ||     ||   ||  |#,//' //||\\\\ '\\\\,`\\,
;;         |  ||   ||     ||   ||  |-|/| ||_||_|| |\\|_ _|
;;         |  ||   ||   = ||   ||  |=  | |.----.|=|___]
;;         |= ||___|| =   ||___|| =|  =| ||    || | ||
;;         | [_______]   [_______] |--.| ||____|| | ||
;;         ;_______=______=_____ __;   |[________]| ||
;;       ,/'#    #   #      #       #  '----------''\\|
;;     ,/'    #        #       #         #     #   # '\\,
;;   ,/'___#____#__#_____#___#_______#_______#____#___#'\\,
;;   `\"\"[____________________________________________]|\"\"`
;;      _[_|   .-----.  =-       ___________    ||_]_||
;;     |  _| .\",-\"|\"-,',   () = |.--..-..--.| = |_  |||
;;     |_/ |/ /_\\_|_/_\\ \\ /__\\  ||__||_||__||   | \\_|\\\\
;;     (_) || .-------. | |  |  |.--..-..--.|  =| (_) ||
;;     / \\ || |       | | |()|  ||__||_||__||   | / \\ ||
;;     \\_/ || '-------' |  )(   |___________|   | \\_/ ||
;;     (_) ||.---------.|  \\/   |.---------.|=  | (_) ||
;;     / \\ |||   ___   ||    =  ||         ||   | / \\ ||
;;     \\_/ |||  {___}  ||       ||         ||   | \\_/ ||
;;     (_) |||  ((_))  || -=   =||_________|| = |_(_)_//
;;     / \\ |||   '-'   ||   _ .-'-----------'-. | / \\__\\
;;     \\_/_|||       ()||  [_]\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"[_]\\_/\\\\\\\\
;;    [ __ ]||         || =| |==.==.==.==.==.==| |__]|||||
;;    j|  ||||         ||  | |  |  |  |  |  |  | |  |====|
;;    g|__|||;).       ||--|_|=='=='=='=='=='==|_|  ||||||
;;   _s____/`---`\\ ____||_.____._____._____.____.|__|////
;;  |     |  9.9  |=====' |    |    /  \\    \\   |    |-'
;;  '------\\ www /---'----'----'---'----'---'---'----'
;;          `---'
;; "
   ;; ))
   (cons 96
         #("          ,-````--,
        ;`         ';,
     _-'    O         '-_
   ;`      .-.           `,
  /       (   )       @   .|
 |   ()    `-`    o         \\
/                          .|
|          *     ,--,      .|
|               |    |      |
| *              '--'       /
\\        o       @         |
 |                   o   ../
 \\,.               o     ,
   --.    @            .--
     \\;,,.         .,,;/
         `--,____,-`



















")))
  "Background strings."
  :type '(repeat (alist :key-type (integer :tag "Offset from left window edge")
                        :value-type (string :tag "String"))))

(defcustom autumn-pile-strings
  '((0.0 . " ")
    (0.03125 . ".")
    (0.0625 . "_")
    (0.125 . "▁")
    (0.25 . "▂")
    (0.375 . "▃")
    (0.5 . "▄")
    (0.625 . "▅")
    (0.75 . "▆")
    (0.875 . "▇")
    (1.0 . "█"))
  "Alist mapping autumn pile percentages to characters.
Each position in the buffer may have an accumulated amount of
autumn, displayed with these characters."
  :type '(alist :key-type float
                :value-type string))

(defcustom autumn-storm-interval
  (lambda ()
    (max 1 (random 100)))
  "Ebb or flow the storm every this many frames."
  :type '(choice integer function))

(defcustom autumn-storm-initial-factor
  (lambda ()
    (cl-random 0.07))
  "Begin autumning at this intensity."
  :type '(choice number function))

(defcustom autumn-storm-wind-max 0.3
  "Maximum wind velocity."
  :type 'float)

;;;; Commands

;;;###autoload
(defun autumn (&optional manual)
  "Watch the leaves!
If already viewing, stop.  If MANUAL (interactively, with
prefix), advance autumn frames manually by pressing \"SPC\"."
  (interactive "P")
  (with-current-buffer (get-buffer-create "*autumn*")
    (if autumn-timer
        (progn
          (cancel-timer autumn-timer)
          (setq autumn-timer nil))
      ;; Start autumning.
      (switch-to-buffer (current-buffer))
      (buffer-disable-undo)
      (erase-buffer)
      (remove-overlays)
      (toggle-truncate-lines 1)
      (use-local-map (make-sparse-keymap))
      (local-set-key (kbd "SPC") (lambda ()
                                   (interactive)
                                   (autumn--update-buffer (current-buffer))))
      (setq-local cursor-type nil)
      (setf autumn-window-width (if autumn-show-background
                                  (max (window-text-width (get-buffer-window (current-buffer) t))
                                       (cl-loop for (offset . background) in autumn-backgrounds
                                                maximizing (+ (cl-loop for line in (split-string background "\n")
                                                                       maximizing (string-width line))
                                                              offset)))
                                (window-text-width (get-buffer-window (current-buffer) t)))
            ;; FIXME: Assumes that the backgrounds are less than the window height.
            autumn-window-height (window-text-height (get-buffer-window (current-buffer) t))
            autumn-leaves nil
            autumn-storm-factor (cl-etypecase autumn-storm-initial-factor
                                (function (funcall autumn-storm-initial-factor))
                                (number autumn-storm-initial-factor))
            autumn-storm-frames 0
            autumn-storm-wind 0
            autumn-storm-reset-frame (cl-etypecase autumn-storm-interval
                                     (function (funcall autumn-storm-interval))
                                     (number autumn-storm-interval))
            ;; Not sure how many of these are absolutely necessary,
            ;; but it seems to be working now.
            indicate-buffer-boundaries nil
	    fringe-indicator-alist '((truncation . nil)
				     (continuation . nil))
	    left-fringe-width 0
	    right-fringe-width 0)
      (goto-char (point-min))
      (save-excursion
        (dotimes (_i autumn-window-height)
          ;; Fill buffer with spaces up to window size.
          (insert (make-string autumn-window-width ? )
                  "\n"))
        (when autumn-show-background
          (pcase-dolist (`(,col . ,string) autumn-backgrounds)
	    (autumn-insert-background :start-line -1 :start-col col :string string))))
      (setq autumn-leaf-positions (save-excursion (autumn-find-leaf-positions)))
      (unless manual
        (setq autumn-timer
              (run-at-time nil autumn-rate (apply-partially #'autumn--update-buffer (get-buffer-create "*autumn*"))))
        (setq-local kill-buffer-hook (lambda ()
                                       (when autumn-timer
					 (cancel-timer autumn-timer)
					 (setq autumn-timer nil))))))))

;;;; Functions


(defun autumn-find-leaf-positions ()
  (if (not (re-search-forward (rx (or "&" "*")) nil t))
      nil
    (let ((pos (1- (point)))
          (beg (line-beginning-position))
          (rest (autumn-find-leaf-positions)))
      (if (<= (- pos beg) 67)
          (cons pos rest)
        rest))))

(defsubst autumn-clamp (min number max)
  "Return NUMBER clamped to between MIN and MAX, inclusive."
  (max min (min max number)))


(defsubst autumn-leaf-color-bias (red green blue raw)
  "Bias color RAW randomly towards RED, GREEN, and BLUE"
  (color-rgb-to-hex
   (autumn-clamp 0 (+ raw red (cl-random 0.4)) 1) 
   (autumn-clamp 0 (+ raw green) 1)
   (autumn-clamp 0 (+ raw (- blue (cl-random 0.4))) 1)
   2))


(defsubst autumn-leaf-color (mass)
  "Return color name for a leaf having MASS."
  (setf mass (autumn-clamp 0 mass 100))
  (let ((raw (/ (+ mass 155) 255.0)))
    (cond
     ((zerop (random 5)) (autumn-leaf-color-bias 0.5 -0.3 -0.4 raw)) ;; red
     ((zerop (random 5)) (autumn-leaf-color-bias 0.3 0.2 -0.2 raw)) ;; yellow
     (t (autumn-leaf-color-bias 0.3 -0.2 -0.2 raw))))) ;; orangish

(defsubst autumn-leaf-mass-string (mass)
  "Return string for leaf having MASS."
  (propertize (pcase mass
                ((pred (< 6.0)) "#")
                ((pred (< 1.0)) "*")
                ((pred (< 0.1)) ".")
                (_ "."))
              'face (list :foreground (autumn-leaf-color mass))))

(defsubst autumn-leaf-within-sides-p (leaf)
  "Return non-nil if LEAF is within window's sides."
  (and (<= 0 (autumn-leaf-x leaf))
       ;; FIXME: Eventually go up to the width rather than 2 less.
       (< (autumn-leaf-x leaf) (- autumn-window-width 2))))

(defsubst autumn-leaf-pos (leaf)
  "Return buffer position of LEAF."
  (save-excursion
    (goto-char (point-min))
    (forward-line (autumn-leaf-y leaf))
    (forward-char (autumn-leaf-x leaf))
    (point)))

(defsubst autumn-leaf-pos-below (leaf)
  "Return buffer position below LEAF, or nil."
  (save-excursion
    (goto-char (autumn-leaf-pos leaf))
    (ignore-errors
      (let ((col (current-column)))
        (forward-line 1)
        (forward-char col)
        (point)))))

(defsubst autumn-leaf-landed-at (leaf)
  "Return buffer position LEAF landed at, or t if outside buffer."
  ;; FIXME: Eventually use full height rather than one less.
  (or (when (>= (autumn-leaf-y leaf) (1- autumn-window-height))
        ;; Leaf hit bottom of buffer.
        (if (autumn-leaf-within-sides-p leaf)
            ;; Leaf within horizontal limit of buffer: return buffer position.
            (autumn-leaf-pos leaf)
          ;; Leaf outside horizontal limit of buffer: return t.
          t))
      (when-let ((pos-below (when (autumn-leaf-within-sides-p leaf)
                              (autumn-leaf-pos-below leaf))))
        ;; A position exists below the leaf and within the buffer.
        (when (not (or (equal ?  (char-after pos-below))
                       (equal "saddle brown" ;; let leaves fall through the tree, (dependent on color)
                              (plist-get
                               (get-text-property pos-below 'face)
                               :foreground))
                       (seq-contains autumn-leaf-positions pos-below))) ;; let leaves fall through other leaves
          ;; That position is not empty (i.e. not a space): return that position.
          pos-below))))

(defun autumn--update-buffer (buffer)
  "Update autumn in BUFFER."
  (with-current-buffer buffer
    (when (>= (cl-incf autumn-storm-frames) autumn-storm-reset-frame)
      (setf autumn-storm-reset-frame (cl-etypecase autumn-storm-interval
                                     (function (funcall autumn-storm-interval))
                                     (number autumn-storm-interval))
            autumn-storm-factor (autumn-clamp 0.01
                                          (+ autumn-storm-factor
                                             (if (zerop (random 2))
                                                 -0.01 0.01))
                                          0.15)
            autumn-storm-wind (autumn-clamp (- autumn-storm-wind-max)
                                        (+ autumn-storm-wind
                                           (if (zerop (random 2))
                                               -0.05 0.05))
                                        autumn-storm-wind-max)
            autumn-storm-frames 0))
    (let ((num-new-leaves (if (< (cl-random 1.0) autumn-storm-factor)
                              1 0)))
      (unless (zerop num-new-leaves)
        ;; FIXME: This can only produce one leaf per frame, which isn't quite what I want.
        (setf autumn-leaves (append autumn-leaves (autumn-new-leaves num-new-leaves))))
      (setq autumn-leaves
            (cl-loop for leaf in autumn-leaves
                     for new-leaf = (autumn-leaf-update leaf)
                     when new-leaf
                     collect new-leaf)))
    (when autumn-debug
      (setq mode-line-format (format "Leaves:%s  Frames:%s  Factor:%s  Wind:%s"
                                     (length autumn-leaves) autumn-storm-frames autumn-storm-factor autumn-storm-wind)))))


(defun autumn-random-leaf-pos ()
  "Get pos for a random leaf in the tree. Also sometimes deletes
the existing leaf from the tree."
  (save-excursion
    (goto-char (seq-random-elt autumn-leaf-positions))
    (when (zerop (random autumn-leaf-disappear-factor))
      (delete-char 1)
      (insert " "))
    `(,(- (point) (line-beginning-position)) . ,(line-number-at-pos (point)))))


(defun autumn-new-leaves (num)
  "Return NUM new leaves starting at the tree.
Also draws each leaf."
  (cl-loop for i from 0 to num
           for pos = (autumn-random-leaf-pos)
           for mass = (float (* autumn-storm-factor (random 100)))
           for leaf = (make-autumn-leaf :x (car pos) :y (1+ (cdr pos)) :mass mass :string (autumn-leaf-mass-string mass))
           do (autumn-leaf-draw leaf)
           collect leaf))

(defun autumn-leaf-update (leaf)
  "Return updated LEAF, or nil if it landed.
Piles leaf if it lands within the buffer."
  (unless (zerop autumn-storm-wind)
    ;; Wind.
    (when (<= (cl-random autumn-storm-wind-max) (abs autumn-storm-wind))
      (cl-incf (autumn-leaf-x leaf) (round (copysign 1.0 autumn-storm-wind)))))
  (when (and (> (random 100) (autumn-leaf-mass leaf))
             ;; Easiest way to just reduce the chance of X movement is to add another condition.
             (> (random 3) 0))
    ;; Random floatiness.
    (cl-incf (autumn-leaf-x leaf) (pcase (random 2)
                                    (0 -1)
                                    (1 1))))
  (when (> (random 100) (/ (- 100 (autumn-leaf-mass leaf)) 3))
    ;; Gravity.
    (cl-incf (autumn-leaf-y leaf)))
  (if-let ((landed-at (autumn-leaf-landed-at leaf)))
      (progn
        ;; Leaf hit bottom of window.
        (when (numberp landed-at)
          ;; Landed at position within window: add to pile.
          (autumn-pile leaf landed-at))
        (when (autumn-leaf-overlay leaf)
          (delete-overlay (autumn-leaf-overlay leaf)))
        ;; No more leaf.
        nil)
    ;; Redraw leaf
    (autumn-leaf-draw leaf)
    ;; Return moved leaf
    leaf))

(defun autumn-pile (leaf pos-below)
  "Pile LEAF having landed at POS-BELOW."
  (cl-labels ((landed-at (leaf pos-below)
                         (let* ((mass-at-pos (or (get-text-property pos-below 'autumn (current-buffer)) 0)))
                           (pcase mass-at-pos
                             ((pred (< 100))
                              ;; Position has more than 100 mass: land
                              ;; above it and return 0 mass.
                              (cons (autumn-leaf-pos leaf) 0))
                             (_ (cons pos-below mass-at-pos))))))
    (pcase-let* ((`(,pos . ,ground-autumn-mass) (landed-at leaf pos-below))
		 (ground-autumn-mass (+ ground-autumn-mass (/ (autumn-leaf-mass leaf) autumn-pile-factor)))
		 (char (or (alist-get (/ ground-autumn-mass 100) autumn-pile-strings nil nil #'>)
                           (alist-get 1.0 autumn-pile-strings nil nil #'eql)))
		 (color (pcase ground-autumn-mass
			  ((pred (<= 100)) (autumn-leaf-color 100))
			  (_ (autumn-leaf-color ground-autumn-mass))))
		 (ground-autumn-string (propertize char 'face (list :foreground color))))
      (when ground-autumn-string
        (setf (buffer-substring pos (1+ pos)) ground-autumn-string))
      (add-text-properties pos (1+ pos) (list 'autumn ground-autumn-mass) (current-buffer)))))

(defun autumn-leaf-draw (leaf)
  "Draw LEAF when it's within the buffer.
If not, delete its overlay."
  (if-let ((pos (when (autumn-leaf-within-sides-p leaf)
                  (autumn-leaf-pos leaf))))
      ;; Leaf within window: draw it.
      (if (autumn-leaf-overlay leaf)
          (move-overlay (autumn-leaf-overlay leaf) pos (1+ pos))
        (setf (autumn-leaf-overlay leaf) (make-overlay pos (1+ pos)))
        (overlay-put (autumn-leaf-overlay leaf) 'display (autumn-leaf-string leaf)))
    ;; Leaf outside window: delete its overlay.
    (when (autumn-leaf-overlay leaf)
      (delete-overlay (autumn-leaf-overlay leaf)))))

(cl-defun autumn-insert-background (&key string (start-line 0) (start-col 0))
  "Insert STRING at START-LINE and START-COL."
  (let* ((lines (split-string string "\n"))
         (height (length lines))
         (start-line (pcase start-line
                       (-1 (- (line-number-at-pos (point-max)) height))
                       (_ start-line))))
    (cl-assert (>= (line-number-at-pos (point-max)) height))
    (when (string-empty-p (car (last lines)))
      ;; Remove final line break.
      (setf lines (butlast lines)))
    (save-excursion
      (goto-char (point-min))
      (forward-line start-line)
      (cl-loop for line in lines
	       for pos = (+ start-col (point))
               do (progn
                    (setf (buffer-substring pos (+ pos (length line))) line)
                    (forward-line 1))))))

;;;; Footer

(provide 'autumn)

;; Ensure that the before-save-hook doesn't, e.g. delete-trailing-whitespace,
;; which breaks the background string.

;; Local Variables:
;; before-save-hook: nil
;; End:

;;; autumn.el ends here
